from django.test import TestCase
from django.db.utils import OperationalError
from .models import Attendee
from events.models import Conference, Location, State
import datetime


class FeatureTests(TestCase):
    def create_state(self):
        return State.objects.create(name="California", abbreviation="CA")

    def create_location(self):
        return Location.objects.create(
            name="Place",
            city="Los Angeles",
            room_count=5,
            state=self.create_state(),
        )

    def create_conference(self):
        return Conference.objects.create(
            name="Test Conference",
            starts=datetime.datetime.now(),
            ends=datetime.datetime.now() + datetime.timedelta(days=1),
            max_presentations=1,
            max_attendees=1,
            location=self.create_location(),
        )

    def create_attendee(self):
        return Attendee.objects.create(
            email="test@example.com",
            name="Test Attendee",
            conference=self.create_conference(),
        )

    def test_create_badge(self):
        try:
            attendee = self.create_attendee()
            attendee.create_badge()
            self.assertIsNotNone(attendee.badge)
        except OperationalError:
            self.fail("Failed to create badge")
