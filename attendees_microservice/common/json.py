from django.db.models import QuerySet
from json import JSONEncoder
from datetime import datetime


class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet):
            return list(o)
        return super().default(o)


class DateEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()
        return super().default(o)


class ModelEncoder(DateEncoder, QuerySetEncoder, JSONEncoder):
    encoders = {}

    def default(self, o):
        if not isinstance(o, self.model):
            return super().default(o)

        d = {}
        if hasattr(o, "get_api_url"):
            d["href"] = o.get_api_url()

        d.update(
            {
                property: (
                    self.encoders[property].default(getattr(o, property))
                    if property in self.encoders
                    else getattr(o, property)
                )
                for property in self.properties
            }
        )
        d.update(self.get_extra_data(o))
        return d

    def get_extra_data(self, o):
        return {}
