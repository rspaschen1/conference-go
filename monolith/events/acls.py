import requests
import json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    url = "https://api.pexels.com/v1/search"
    params = {"per_page": 1, "query": f"{city}, {state}"}
    response = requests.get(url, params=params, headers=headers)
    if response.status_code == 200:
        data = json.loads(response.content)
        image_url = data['photos'][0]['src']['original']
        return {"picture_url": image_url}
    else:
        print("Error: API call failed", response.status_code)
        print("Error message:", response.text)
        return None


def get_weather_data(city, state):
    geo_url = "https://api.openweathermap.org/geo/1.0/direct"
    geo_params = {
        "q": f"{city}, {state},US",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY,
    }
    geo_response = requests.get(geo_url, params=geo_params)
    if geo_response.status_code == 200:
        geo_data = json.loads(geo_response.content)
        latitude = geo_data[0]["lat"]
        longitude = geo_data[0]["lon"]
        
        weather_url = "https://api.openweathermap.org/data/2.5/weather"
        weather_params = {
            "lat": latitude,
            "lon": longitude,
            "appid": OPEN_WEATHER_API_KEY,
            "units": "imperial"
        }
        weather_response = requests.get(weather_url, params=weather_params)
        if weather_response.status_code == 200:
            weather_data = json.loads(weather_response.content)
            return {
                "temperature": weather_data["main"]["temp"],
                "description": weather_data["weather"][0]["description"]
            }
        else:
            print("Error: API call failed", weather_response.status_code)
            print("Error message:", weather_response.text)
            return None
    else:
        print("Error: API call failed", geo_response.status_code)
        print("Error message:", geo_response.text)
        return None
