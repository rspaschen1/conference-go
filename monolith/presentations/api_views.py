from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from django.views.decorators.csrf import csrf_exempt
import json
from events.models import Conference
from .models import Presentation
from .encoders import PresentationListEncoder, PresentationDetailEncoder


@csrf_exempt
@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):
    if request.method == "GET":
        presentations = Presentation.objects.all()
        return JsonResponse(
            {"presentations": presentations},
            encoder=PresentationListEncoder,
        )
    else:
        data = json.loads(request.body)
        try:
            conference = Conference.objects.get(id=data["conference"])
            data["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference_id"}, status=400
            )
        presentation = Presentation.create(**data)
        return JsonResponse(
            presentation, encoder=PresentationListEncoder, safe=False
        )


@csrf_exempt
@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_presentation(request, id):
    if request.method == "GET":
        presentation = Presentation.objects.get(id=id)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Presentation.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        data = json.loads(request.body)
        try:
            conference = Conference.objects.get(id=data["conference"])
            data["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference_id"}, status=400
            )
        Presentation.objects.filter(id=id).update(**data)
        presentation = Presentation.objects.get(id=id)
        return JsonResponse(
            presentation, encoder=PresentationDetailEncoder, safe=False
        )
